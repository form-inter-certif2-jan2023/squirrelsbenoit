
<!-- README.md is generated from README.Rmd. Please edit that file -->

# squirrelsbenoit

<!-- badges: start -->
<!-- badges: end -->

The goal of squirrelsbenoit is to study squirrels of New York.

## Installation

You can install the development version of squirrelsbenoit like so:

``` r
install.packages("~/squirrelsbenoit_0.0.0.9000.tar.gz", repos = NULL, type = "source")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(squirrelsbenoit)
## basic example code

get_message_fur_color("Grey")
#> We will focus on Grey squirrels
get_message_fur_color("Cinnamon")
#> We will focus on Cinnamon squirrels

check_primary_color_is_ok(string = c("Gray", "Cinnamon", "Black", NA))
#> [1] TRUE
```

What is special about using `README.Rmd` instead of just `README.md`?
You can include R chunks like so:
